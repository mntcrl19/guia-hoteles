//ACTIVAR TOOLTIP / POPOVER
$(function() {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
})


//Modificar las funciones del CAROUSEL-->
$(function() {
    $('.carousel').carousel({
        interval: 2000
    });
})


//Editando un Evento del componente MODAL
$(function() {
    $('#contacto-hotel').on('show.bs.modal', function(e) {
        console.log('El modal se está ejecutando.')

        //Cuando se abra el Modal se cambia el color del botón
        //.remoceClass remueve la clase 'btn-outline-success' del botón
        $('#contactoBtn').removeClass('btn-outline-success')
            //.addClass agrega una clase nueva al botón
        $('#contactoBtn').addClass('btn-primary')
            //.prop accede a las propiedades del elemento seleccionado, esta funcion desabilita el botón despues de realizar la acción
        $('#contactoBtn').prop('disabled', true)
    })
    $('#contacto-hotel').on('shown.bs.modal', function(e) {
        console.log('El modal se mostró.')
    })
    $('#contacto-hotel').on('hide.bs.modal', function(e) {
        console.log('El modal se oculta.')
    })
    $('#contacto-hotel').on('hidden.bs.modal', function(e) {
        console.log('El modal se ocultó.')
        $('#contactoBtn').prop('disabled', false)
    })
})